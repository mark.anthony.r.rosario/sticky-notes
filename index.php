<?php

  $apiHost = getenv('API_URL');
  $pusherAppKey = 'eaeb26d5342387203669';
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Taskboard</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="https://js.pusher.com/4.0/pusher.min.js"></script>
    <script>

        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = true;

        var pusher = new Pusher("<?= $pusherAppKey ?>", {
          encrypted: true
        });

        var channel = pusher.subscribe('task-channel');
        channel.bind('task-update', function(data) {
          showAllTasks();
        });
      </script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="index.html">Taskboard</a>
            </div>
            <!-- Top Menu Items -->

            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <div class="form-group">
                            <label for="email">Task</label>
                            <textarea cols=20 rows=7 class="form-control" id="task-name" placeholder="write your tasks here..."></textarea>
                        </div>
                        <button onclick="createTask($('#task-name').val())" type="submit" class="col-md-12 btn btn-primary">
                            <i class="fa fa-fw fa-file"></i> New Task
                        </button>
                        <br /><br />
                        <button onclick="clearAllTasks()" type="submit" class="col-md-12 btn btn-danger">
                            <i class="fa fa-fw fa-trash"></i> Clear All
                        </button>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">


                <!-- /.row -->



                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i> Tasks Panel</h3>
                            </div>
                            <div class="panel-body">
                                <div class="list-group" id="task-list">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

   <script>
     function createTask(taskName) {
        $.ajax({
          url: "<?= $apiHost ?>/tasks",
          type: 'POST',
          crossDomain: true,
          data: {taskName:taskName},
          success: function(result) {
                console.log(result);
                showAllTasks();
            }
        });
      }

      function clearAllTasks() {
        $.ajax({
            url: "<?= $apiHost ?>/tasks",
            type: 'DELETE',
            crossDomain: true,
            success: function(result) {
                console.log(result);
                showAllTasks();
            }
        });
      }

      function showAllTasks() {
        $.ajax({
            url: "<?= $apiHost ?>/tasks",
            type: 'GET',
            crossDomain: true,
            dataType: "json",
            success: function(result) {
                console.log(result);
                clearTaskList();
                $.each(result, function( index, taskname) {
                  appendTask(taskname);
                });
            }
        });
      }

      function appendTask(taskname) {
        $('#task-list').show();
        taskHtml = '<a class="list-group-item"><span class="badge">timestamp here</span><i class="fa fa-fw fa-file"></i> ' + taskname + '</a>';
        $('#task-list').append(taskHtml);
      }

      function clearTaskList() {
        $('#task-list').hide();
        $('#task-list').html('');
      }
   </script>
   <script>
      $(document).ready(function(){
        showAllTasks();
      });
   </script>

</body>

</html>
